import {Fragment} from 'react'

import Banner from './../components/Banner'

// import CourseCard from './../components/CourseCard'
import Highlights from './../components/Highlight'

export default function Home(){
	return(
		// render navbar, banner & footer in the webpage via home.js
		<Fragment>
			
			<Banner/>
			
			<Highlights/>
		{/*	<CourseCard/>*/}
			

		</Fragment>	
	)
}