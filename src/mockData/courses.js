
let coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Illo, hic eum, magni doloribus dolores nesciunt inventore. Sapiente asperiores iste dolore voluptatum veniam dolorem repellat minus expedita reprehenderit quisquam. Temporibus, aperiam.",
		price: 25000,
		onOffer: true
	},
	{		id: "wdc002",
		name: "Python-Django",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Illo, hic eum, magni doloribus dolores nesciunt inventore. Sapiente asperiores iste dolore voluptatum veniam dolorem repellat minus expedita reprehenderit quisquam. Temporibus, aperiam.",
		price: 35000,
		onOffer: true
	},
	{		id: "wdc003",
		name: "Java-Springboot",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Illo, hic eum, magni doloribus dolores nesciunt inventore. Sapiente asperiores iste dolore voluptatum veniam dolorem repellat minus expedita reprehenderit quisquam. Temporibus, aperiam.",
		price: 45000,
		onOffer: true
	},
	{		id: "wdc004",
		name: "Nodejs-ExpressJS",
		description: "Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Illo, hic eum, magni doloribus dolores nesciunt inventore. Sapiente asperiores iste dolore voluptatum veniam dolorem repellat minus expedita reprehenderit quisquam. Temporibus, aperiam.",
		price: 55000,
		onOffer: false
	}
]

export default coursesData