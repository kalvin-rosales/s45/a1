import {useState, useEffect} from 'react'
import {Card, Row, Col, Button} from "react-bootstrap"

export default function CourseCard({courseProp}) {
	 // console.log(props) //object
	/* { courseProp: {
				id: "wdc001",
				name: "PHP-Laravel",
				description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea tenetur illo, delectus doloribus consequuntur facere exercitationem laborum blanditiis magnam sequi iste",
				price: 25000,
				onOffer: true
	 		}
	 	}
	*/
	//console.log(courseProp)
	/*
		{
			id: "wdc001",
			name: "PHP-Laravel",
			description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea tenetur illo, delectus doloribus consequuntur facere exercitationem laborum blanditiis magnam sequi iste",
			price: 25000,
			onOffer: true
 		}
	*/

	const [count, setCount] = useState(0)
	const [seat, setSeat] = useState(30)

	let {name, description, price} = courseProp

	useEffect(() => {
		console.log('render')
	}, [count])
	// console.log(name)
	// console.log(description)
	// console.log(price)

	const handleClick = () => {
		//console.log(`I'm clicked`, count + 1)
		//count++
		setCount(count => Math.min(count + 1, 30))
		setSeat(seat => Math.max(seat - 1,0))
		console.log(seat);

		if(count === 30){
			
			return alert(`No more seats`)
		}
		// if(count === 30){
		// 	return alert(`No more seats`);

		// 	seat: 0;
		// 	count: 30;
		//}
	}

	return(
		<Card className="m-5">
		  <Card.Body>
		    <Card.Title>{name}</Card.Title>
		    <Card.Subtitle>Description:</Card.Subtitle>
		    <Card.Text>
		      {description}
		    </Card.Text>
		    <Card.Subtitle>Price:</Card.Subtitle>
		    <Card.Text>
		    	{price}
		    </Card.Text>
		    <Card.Text>Count: {count}</Card.Text>
		    <Button className="btn-info" onClick={handleClick}>Enroll</Button>
		  </Card.Body>
		</Card>
	)
}
