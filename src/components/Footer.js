

export default function Footer(){
	return(
		<div className="bg-info fixed-bottom text-white d-flex justify-content-center align-items-center" style={{height:'10vh', margintTop:'20px'}}>
		  <p className="m-0 font-weight-bold">Kalvin Philip Rosales &#64; Course Booking | Zuitt Coding Bootcamp  &#169;</p>
		</div>
		)
}