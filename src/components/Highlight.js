import {Card, Row, Col, Button} from "react-bootstrap"
import { Fragment } from "react";

export default function Highlights(){

	return(
		<Row className="m-5">
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Learn From Home</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing elit. Error ex modi similique tempora veniam aliquid delectus assumenda fugit, magni quam, vel sequi molestiae accusantium cumque possimus ducimus sit earum minus?
				    </Card.Text>
				 
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Study now, Pay later</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing elit. Error ex modi similique tempora veniam aliquid delectus assumenda fugit, magni quam, vel sequi molestiae accusantium cumque possimus ducimus sit earum minus?
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card>
				  <Card.Body>
				    <Card.Title>Be Part of Our Community</Card.Title>
				    <Card.Text>
				      Lorem ipsum dolor sit amet consectetur adipisicing elit. Error ex modi similique tempora veniam aliquid delectus assumenda fugit, magni quam, vel sequi molestiae accusantium cumque possimus ducimus sit earum minus?
				    </Card.Text>
			
				  </Card.Body>
				</Card>
			</Col>
		</Row>



		)
}